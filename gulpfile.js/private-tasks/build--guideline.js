const { src, dest, series, lastRun, parallel, watch } = require('gulp');
const { join } = require('path');
const { addPath } = require('../utils');
const webpack = require('webpack-stream');
const vinylNamed = require('vinyl-named');
const sass = require('gulp-sass')(require('sass'));
const pug = require('gulp-pug');
const htmlReplace = require('gulp-html-replace');
const cached = require('gulp-cached');
const bulkSass = require('organizze-gulp-sass-bulk-import');
const autoprefixer = require('gulp-autoprefixer');
const lintScripts = require('./lint--scripts');
const printResults = require('./print--results');
const reload = require('./reload');
const rename = require('gulp-rename');
const cleanCss = require('gulp-clean-css');
const uglify = require('gulp-uglify');
const useminOpts = require('../config/usemin');

const { min } = require('../config/rename');
const { srcGuideline, output } = require('../config/directories');
const { list, handleError } = require('../utils/errors');
const del = require('del');

const nodeEnv = process.env.NODE_ENV;
const isDevelopment = nodeEnv !== 'production';

//------- Directory --------
const srcScript = `${srcGuideline}$scripts/`;
const filesJsGuideline = `${srcScript}*.js`;
const outputScript = `${output}guideline/js/`;

const srcStyle = `${srcGuideline}$styles/`;
const filesScssGuideline = addPath(
  srcStyle,
  '**/*.scss',
  ['$*/**/*.scss', '_*/**/*.scss']
);
const outputStyle = `${output}guideline/css/`;
const sassOpts = {outputStyle: 'expanded'};

const filesPugGuideline = addPath(srcGuideline, '*.pug', ['$*/**/*', '_*/**/*']);
const outputView = `${output}guideline/`;
///// ------ //////

//-------- JSON Data --------
const srcData = `${srcGuideline}data/*.json`;
const outputData = `${output}guideline/data/`;

function copyDataGuideline () {
  return src(srcData, {
    since: lastRun(copyDataGuideline)
  }).pipe(dest(outputData));
}
copyDataGuideline.displayName = 'copy:data-guideline';

//-------- JS Compiler --------
// Webpack Config
const option = {
  mode: 'none',
  context: join(__dirname, '../../', output),
  output: {
    path: join(__dirname, '../../', outputScript),
    filename: '[name].js'
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        loader: 'babel-loader',
        options: {
          presets: ['@babel/preset-env'],
          plugins: [
            [
              '@babel/plugin-transform-runtime', {
                helpers: true,
                regenerator: true
              }
            ],
            [
              '@babel/plugin-proposal-decorators', {
                legacy: true
              }
            ],
            [
              '@babel/plugin-proposal-pipeline-operator', {
                proposal: 'minimal'
              }
            ],
            '@babel/plugin-proposal-class-properties',
            '@babel/plugin-transform-function-name',
            '@babel/plugin-syntax-dynamic-import',
            '@babel/plugin-proposal-optional-chaining'
          ],
          cacheDirectory: true
        }
      }
    ]
  },
  optimization: {
    nodeEnv,
    splitChunks: false,
    flagIncludedChunks: true,
    concatenateModules: true,
    occurrenceOrder: true,
    sideEffects: true
  },
  devtool: isDevelopment && 'source-map'
};

function buildScriptsGuideline (cb) {
  if (!list.isJSValid) {
    return cb();
  }

  return src(filesJsGuideline)
    .pipe(vinylNamed())
    .pipe(webpack(option))
    .on('error', handleError)
    .pipe(dest(outputScript));
}

buildScriptsGuideline.displayName = 'build:scripts-guideline';
///// ------ //////

//-------- CSS Compiler --------
function buildStylesGuideline () {
  return src(filesScssGuideline)
    .pipe(cached('scss'))
    .pipe(bulkSass())
    .on('error', handleError)
    .pipe(sass(sassOpts))
    .on('error', handleError)
    .pipe(autoprefixer())
    .on('error', handleError)
    .pipe(dest(outputStyle));
}

buildStylesGuideline.displayName = 'build:styles-guideline';
///// ------ //////

//-------- PugJS Compiler --------
function buildViewGuideline () {
  return src(filesPugGuideline)
    .pipe(pug({
      pretty: true,
      doctype: 'html'
    }))
    .on('error', handleError)
    .pipe(dest(outputView));
}

buildViewGuideline.displayName = 'build:views-guideline';
exports.buildViewGuideline = buildViewGuideline;

function buildViewGuidelineMin () {
  return src(`${outputView}**/*.html`)
    .pipe(htmlReplace(useminOpts, {
      resolvePaths: true
    }))
    .on('error', handleError)
    .pipe(dest(outputView));
}
buildViewGuidelineMin.displayName = 'build:views-guideline-min';
exports.buildViewGuidelineMin = buildViewGuidelineMin;

///// ------ //////

//-------- Watcher --------
function runWatcherGuideline (cb) {
  const reloadAndShowResults = parallel(printResults, reload);

  //----- HTML
  watch(filesPugGuideline, reload);

  //----- CSS
  watch(filesScssGuideline, series(
    buildStylesGuideline,
    printResults
  )).on('change', () => delete cached.caches.scss);

  //----- JS
  watch(filesJsGuideline, series(
    lintScripts,
    buildScriptsGuideline,
    reloadAndShowResults
  ));

  //----- JSON Data
  watch(srcData, series(
    copyDataGuideline,
    reloadAndShowResults
  ));

  cb();
}
runWatcherGuideline.displayName = 'run:watchers-guideline';
exports.runWatcherGuideline = runWatcherGuideline;

//-------- Minify CSS --------
function minifyStyleGuideline () {
  return src(`${outputStyle}/guideline.css`)
    .pipe(rename(min))
    .pipe(cleanCss())
    .on('error', handleError)
    .pipe(dest(outputStyle));
}
minifyStyleGuideline.displayName = 'minify:styles-guideline';
exports.minifyStyleGuideline = minifyStyleGuideline;

const cleanTempStyleGuideline = () => del(`${outputStyle}guideline.css`);
cleanTempStyleGuideline.displayName = 'clean:temp-styles-guideline';
exports.cleanTempStyleGuideline = cleanTempStyleGuideline;

//-------- Minify JS --------
function minifyScriptGuideline () {
  return src(`${outputScript}/guideline.js`)
    .pipe(rename(min))
    .pipe(uglify({
      mangle: {
        keep_fnames: true
      }
    }))
    .on('error', handleError)
    .pipe(dest(outputScript));
}
minifyScriptGuideline.displayName = 'minify:scripts-guideline';
exports.minifyScriptGuideline = minifyScriptGuideline;

const cleanTempScriptGuideline = () => del(`${outputScript}guideline.js`);
cleanTempScriptGuideline.displayName = 'clean:temp-scripts-guideline';
exports.cleanTempScriptGuideline = cleanTempScriptGuideline;

//--------- Tasks -------
const buildGuideline = series(
  copyDataGuideline,
  buildScriptsGuideline,
  buildStylesGuideline
);

exports.buildGuideline = buildGuideline;
