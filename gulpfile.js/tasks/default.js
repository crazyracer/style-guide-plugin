const { series } = require('gulp');

const cleanOutput = require('../private-tasks/clean--output');
const buildAssets = require('../private-tasks/build--assets');
const printResults = require('../private-tasks/print--results');
const runDevServer = require('../private-tasks/run--dev-server');
const { buildGuideline, runWatcherGuideline } = require('../private-tasks/build--guideline');
const { GUIDE_SWITCH } = require('../config/directories');

const tasks = ( GUIDE_SWITCH ) ? [
  cleanOutput,
  buildAssets,
  buildGuideline,
  printResults,
  runDevServer,
  runWatcherGuideline
] : [
  cleanOutput,
  buildAssets,
  printResults,
  runDevServer,
];

module.exports = series(...tasks);
