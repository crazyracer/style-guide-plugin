# Guideline
Guideline is a package which render all components in the project automatically. It will auto-generate the navigation in each component page, and auto-generate the component's HTML Code for reference.

## Environment
Sutrix Frontend Template v3.0.5

## Includes
```bash
./gulpfile.js/private-tasks/build--guideline.js
./src/locales/guideline.json
./src/guideline/**/*
```

## Installation
1. Open file _./gulpfile.js/config/directories.js_, add more these lines:
```javascript
const srcGuideline = `${src}/guideline/`;
exports.srcGuideline = srcGuideline;

const GUIDE_SWITCH = true;
exports.GUIDE_SWITCH = GUIDE_SWITCH;
```

2. Open file _./gulpfile.js/config/usemin.js_, add more these lines:
```javascript
'css-guideline': `dist/guideline/css/guideline.min.css?_cache=${cache}`,
'js-guideline': `dist/guideline/js/guideline.min.js?_cache=${cache}`,
```
and update this line:
```javascript
redirect: `<meta http-equiv="refresh" content="0;url=./${DEFAULT_LANG}/index.html">`
↓
redirect: `<meta http-equiv="refresh" content="0;url=./guideline/sitemap.html">`
```

3. Open file _./gulpfile.js/tasks/default.js_, add more these lines:
```javascript
const { buildGuideline, runWatcherGuideline } = require('../private-tasks/build--guideline');
const { GUIDE_SWITCH } = require('../config/directories');

const tasks = ( GUIDE_SWITCH ) ? [
  cleanOutput,
  buildAssets,
  buildGuideline, // New
  printResults,
  runDevServer,
  runWatcherGuideline // New
] : [
  // Current tasks without Guideline
];

module.exports = series(...tasks);
```

4. Open file _./gulpfile.js/tasks/build.js_, add more these lines:
``` javascript
const { buildGuideline,
  minifyStyleGuideline,
  cleanTempStyleGuideline,
  minifyScriptGuideline,
  cleanTempScriptGuideline,
  buildViewGuideline,
  buildViewGuidelineMin } = require('../private-tasks/build--guideline');
const { GUIDE_SWITCH } = require('../config/directories');

const tasks = ( GUIDE_SWITCH ) ? [
  cleanOutput,
  buildAssets,
  bundleScripts,
  cleanTempScripts,
  backupChunkScripts,
  minifyChunkScripts,
  bundleStyles(),
  // bundleStyles(true),
  cleanTempStyles,
  minifyStyles,
  buildViews,
  buildViewsMin,
  buildGuideline, // New
  minifyStyleGuideline, // New
  cleanTempStyleGuideline, // New
  minifyScriptGuideline, // New
  cleanTempScriptGuideline, // New
  buildViewGuideline, // New
  buildViewGuidelineMin, // New
  printResults
] : [
  // Current tasks without Guideline
]

```

5. Open file _./server/index.js_, update these lines:
```javascript
const { srcView, output } = require('../gulpfile.js/config/directories');
↓
const { srcView, srcGuideline, output } = require('../gulpfile.js/config/directories');
```

```javascript
app.set('views', srcView);
↓
app.set('views', [srcView, srcGuideline]);
```

6. Open file _./server/route.js_, update these lines:
```javascript
const { output, srcLocales } = require('../gulpfile.js/config/directories');
↓
const { output, srcLocales, GUIDE_SWITCH } = require('../gulpfile.js/config/directories');
```

```javascript
res.redirect(`${defautLangPath}/index.html`);
↓
if ( GUIDE_SWITCH ) {
  res.redirect('guideline/sitemap.html');
} else {
  res.redirect(`${defautLangPath}/index.html`);
}
```

```javascript
localeLang = _require(`../${srcLocales + lang}.json`);
↓
if (lang !== 'guideline') {
  localeLang = _require(`../${srcLocales + lang}.json`);
}
```

## Usage
1. For getting HTML Markup of Component
In each component, please add the below information to Component which you want to get HTML Structure:
- Add class "j-gd-html-markup"
- Add attribute [data-gd-html-title= "HTML Markup for Gmap"]
Ex:
``` html
<div class="cmp-gmap j-gd-html-markup" data-gd-html-title= "HTML Markup for Gmap">
  <div class="cmp-gmap__wrapper">
  ...
  </div>
</div>
```

2. Update Guideline List
The data of Guideline List is created by JSON file:
/src/views/guideline/data/data-list.json
``` json
{
  "directLink": false,                  // True: Direct link to Component Page. False: Embed Component Page
  "dataList": [
    {
      "icon": "description",            // Google Material Icon
      "name": "Page",                   // Title of Parent List
      "list": [{                        // List Page/Component
        "id": "P01",                      // ID of Page/Component
        "icon": "home",                   // Google Material Icon
        "title": "Homepage",              // Page/Component Title
        "href": "en/index.html"           // Link to Page/Component
      }]
    }, {
      "icon": "extension",
      "name": "Component",
      "list": [{
        "id": "C01",
        "icon": "language",
        "title": "Gmap",
        "href": "en/components/gmap.html"
      }]
    }
  ]
}

```
Please refer to Google Material Icon for getting icon:
https://material.io/resources/icons/?style=baseline

## Command
1. Run it in Development Mode
```bash
npm start
```
http://localhost:[port-number]/guideline/sitemap.html

2. Run it in Production Mode
```bash
npm run build
```
