import buildList from './plugins/_m-build-list';
import heightOfTOP from './plugins/_m-height-of-top';
import menuSidebar from './plugins/_m-menu-sidebar';
import htmlPopup from './plugins/_m-html-popup';
import iFrame from './plugins/_m-iframe';

buildList();
heightOfTOP();
menuSidebar();
htmlPopup();
iFrame();
