const calcHeight = (header, footer) => {
  let headerHeight = header.offsetHeight,
    footerHeight = footer.offsetHeight,
    windowHeight = window.innerHeight,
    mainContent = document.getElementById('main'),
    mainHeight = mainContent.offsetHeight;

  let differenceHeight = windowHeight - headerHeight - footerHeight - 1;

  // Reset and update current height of main Content
  mainContent.removeAttribute('style');
  mainHeight = mainContent.offsetHeight;

  if (mainHeight < differenceHeight) {
    mainContent.style.height = `${differenceHeight}px`;
  } else {
    mainContent.removeAttribute('style');
  }
};

const heightOfTOP = () => {
  let header = document.getElementById('gd-header'),
    footer = document.getElementById('gd-footer');

  if (header && footer) {
    calcHeight(header, footer);

    // Event Handler
    window.addEventListener( 'resize', () => {
      calcHeight(header, footer);
    });
  }
};

export default heightOfTOP;
