const dateModified = (req) => {
  let header = document.getElementById('gd-header');

  if (header) {
    const elDateModified = header.querySelector('.gd-header__modified');

    if (req.readyState === 4 && req.status === 200) {
      let strDate__GMT = req.getResponseHeader('Last-Modified');
      let strDate__TZ = new Date(strDate__GMT)
        .toLocaleString('en-US', { timeZone: 'Asia/Ho_Chi_Minh' });

      elDateModified.innerHTML += strDate__TZ;
    }
  }
};

export default dateModified;
