import findParentBySelector from '../utils/get-closest-element';
import { saveStateToStorage, removeStateFromStorage, loadStateFromStorage } from './_a-save-state';

const appendTransitionDelay = (el) => {
  let mainListItems = el.querySelectorAll('.gd-main-list__main > li');
  let stepBreak = 0.2;

  for (let i = 0; i < mainListItems.length; i++) {
    mainListItems[i].style.transitionDelay = `${stepBreak}s`;
    stepBreak += 0.1;
  }
};

const submitFormToSetIframe = (el, target) => {
  el.addEventListener('submit', function(ev) {
    ev.preventDefault();

    const formWidth = el.querySelector('.j-form-width'),
      formHeight = el.querySelector('.j-form-height');

    if (formWidth && formHeight) {
      target.style.width = `${formWidth.value}px`;
      target.style.height = `${formHeight.value}px`;

      // Save state to SessionStorage
      saveStateToStorage('gd-iframe--width', `${formWidth.value}px`);
      saveStateToStorage('gd-iframe--height', `${formHeight.value}px`);
    }
  });
};

const clickToSetIframe = (el, target) => {

  el.addEventListener('click', function() {
    const elToolbar = findParentBySelector(this, '.gd-main-detail__toolbar'),
      formWidth = elToolbar.querySelector('.j-form-width'),
      formHeight = elToolbar.querySelector('.j-form-height'),
      targetHeight = target.offsetHeight;

    const screenSize = this.getAttribute('data-size');

    // Get actual width & height for j-gd-custom-form
    if (formWidth) {
      formWidth.value = screenSize;
    }
    if (formHeight) {
      formHeight.value = targetHeight;
    }

    // Set width for target
    if (screenSize) {
      target.style.width = `${screenSize}px`;

      // Save state to SessionStorage
      saveStateToStorage('gd-iframe--width', `${screenSize}px`);
      saveStateToStorage('gd-iframe--height', `${targetHeight}px`);
    } else {
      target.removeAttribute('style');
      formHeight.value = null;

      // Remove state from SessionStorage
      removeStateFromStorage('gd-iframe--width');
      removeStateFromStorage('gd-iframe--height');
    }
  });
};

const menuSidebar = () => {
  const mainList = document.querySelector('.j-gd-menu-sidebar');

  if (mainList) {
    appendTransitionDelay(mainList);

    // Open Menu when screenX of Mouse move is 0
    let countZero = 0;
    document.onmousemove = function(ev) {
      // Count how many times the mouse move is Zero on ScreenX
      // Prevent menu is opened immediately when mouse move is Zero once
      (ev.screenX === 0) ? countZero++ : countZero = 0;

      if (countZero > 15 && mainList.className.indexOf('open') === -1) {
        setTimeout(function() {
          mainList.classList.add('open');
        }, 1000);
      }

    };

    // Close Menu when click close button
    const closeBtn = mainList.querySelector('.gd-close__btn');

    closeBtn.addEventListener('click', function() {
      if (mainList.className.indexOf('open') !== -1) {
        mainList.classList.remove('open');
      } else {
        mainList.classList.add('open');
      }
    });

    // Handle set width for iframe
    const elSmallSize = mainList.querySelector('.j-btn-size--s'),
      elMediumSize = mainList.querySelector('.j-btn-size--m'),
      elLargeSize = mainList.querySelector('.j-btn-size--l'),
      elFullSize = mainList.querySelector('.j-btn-size--full'),
      elCustomForm = mainList.querySelector('.j-gd-custom-form'),
      targetIframe = document.getElementById('j-gd-iframe__wrapper');

    if (elSmallSize) {
      clickToSetIframe(elSmallSize, targetIframe);
    }

    if (elMediumSize) {
      clickToSetIframe(elMediumSize, targetIframe);
    }

    if (elLargeSize) {
      clickToSetIframe(elLargeSize, targetIframe);
    }

    if (elFullSize) {
      clickToSetIframe(elFullSize, targetIframe);
    }

    if (elCustomForm) {
      submitFormToSetIframe(elCustomForm, targetIframe);
    }

    // Load state from sessionStorage
    loadStateFromStorage('gd-iframe--width', targetIframe);
    loadStateFromStorage('gd-iframe--height', targetIframe);

    // Handle the HTML Popup
    const elHtmlPopup = mainList.querySelector('.j-btn-html-markup'),
      mainHtmlPopup = document.querySelector('.j-gd-main-popup');

    if (elHtmlPopup && mainHtmlPopup) {
      elHtmlPopup.addEventListener('click', function() {

        // Open HTML Popup
        if (mainHtmlPopup.className.indexOf('open') === -1) {
          mainHtmlPopup.classList.add('open');
        }

        // Close Menu Sidebar
        if (mainList.className.indexOf('open') !== -1) {
          mainList.classList.remove('open');
        }
      });
    }

    // Close Menu & HTML Popup when press esc key
    document.onkeydown = function(ev) {
      const evt = ev || window.event;
      if (evt.keyCode === 27 && mainList.className.indexOf('open') !== -1) {
        mainList.classList.remove('open');
      }

      if (evt.keyCode === 27 && mainHtmlPopup.className.indexOf('open') !== -1) {
        mainHtmlPopup.classList.remove('open');
      }
    };
  }
};

export default menuSidebar;
