const readFile = (file, success, error) => {
  let request = new XMLHttpRequest();
  request.open('GET', file, true);

  request.onload = function() {
    let element = document.createElement( 'html' );
    element.innerHTML = request.responseText;

    let htmlMarkup = element.querySelectorAll('.j-gd-html-markup');

    if (htmlMarkup.length) {
      success( htmlMarkup );
    } else {
      error();
    }
  };

  request.onreadystatechange = (e) => {
    if (request.status === 404 || request.status === 500) {
      // There was a connection error that returned 404 or 500
      error(e);
    }
  };

  request.send();

};

const htmlPopup = () => {
  const mainPopup = document.querySelector('.j-gd-main-popup');
  const path = window.location.href.split('?p=')[1];

  if (mainPopup) {

    // Close Popup when click close button
    const closeBtn = mainPopup.querySelector('.gd-close__btn');

    closeBtn.addEventListener('click', function() {
      if (mainPopup.className.indexOf('open') !== -1) {
        mainPopup.classList.remove('open');
      }
    });

    // Get HTML content of Component file
    if (path) {
      readFile(
        `../${path}`,
        (response) => {
          const target = mainPopup.querySelector('.j-gd-main-popup__target');

          if (target) {

            for (let i = 0; i < response.length; i++) {
              let createHtmlGroup = document.createElement('div');
              createHtmlGroup.classList.add('gd-main-popup__group');

              // 1. Get HTML, then remove the unnecessary classname & attribute
              let getHtmlTitle = response[i].getAttribute('data-gd-html-title');
              response[i].removeAttribute('data-gd-html-title');
              response[i].classList.remove('j-gd-html-markup');
              if (!response[i].getAttribute('class')) {
                response[i].removeAttribute('class');
              }

              // 2. Create Title of Group and append it to createHtmlGroup
              let createHtmlGroup__TITLE = document.createElement('h4');
              createHtmlGroup__TITLE.classList.add('gd-main-popup__ttl');
              createHtmlGroup__TITLE.textContent = getHtmlTitle;
              createHtmlGroup.appendChild(createHtmlGroup__TITLE);

              // 3a. Create Textarea and append it to createHtmlGroup
              let createHtmlGroup__CODE = document.createElement('textarea');
              createHtmlGroup__CODE.classList.add('gd-main-popup__code');
              createHtmlGroup__CODE.name = 'main-popup-code';
              createHtmlGroup__CODE.setAttribute('readonly', 'readonly');

              // 3b. Convert HTML to String
              let tempHtml = document.createElement('div');
              tempHtml.appendChild(response[i]);
              createHtmlGroup__CODE.textContent = tempHtml.innerHTML;
              createHtmlGroup.appendChild(createHtmlGroup__CODE);

              // 4. Create Copy Button
              let createHtmlGroup__BTN = document.createElement('button');
              createHtmlGroup__BTN.classList.add('gd-btn', 'j-btn-html-copy');
              createHtmlGroup__BTN.title = 'Copy HTML';
              let createHtmlGroup__ICON = document.createElement('i');
              createHtmlGroup__ICON.classList.add('material-icons');
              createHtmlGroup__ICON.textContent = 'file_copy';
              createHtmlGroup__BTN.appendChild(createHtmlGroup__ICON);
              createHtmlGroup.appendChild(createHtmlGroup__BTN);

              // 5. Handle Event "click to Copy"
              createHtmlGroup__BTN.addEventListener('click', function() {
                // Select all text inside textarea
                createHtmlGroup__CODE.selectionStart = 0;
                createHtmlGroup__CODE.selectionEnd = createHtmlGroup__CODE.firstChild.length;
                createHtmlGroup__CODE.focus();

                // Do Copy
                if (!createHtmlGroup.classList.contains('copied')) {
                  createHtmlGroup__CODE.select();
                  document.execCommand('copy');

                  createHtmlGroup.classList.add('copied');

                  setTimeout(function() {
                    createHtmlGroup.classList.remove('copied');
                  }, 1500);
                }
              });

              // Append all to target
              target.appendChild(createHtmlGroup);
            }

          }

        }, () => {
          console.error('fail');
        }
      );
    }

  }
};

export default htmlPopup;
