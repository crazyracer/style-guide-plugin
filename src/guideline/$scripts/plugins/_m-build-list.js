import navActive from './_a-nav-active';
import dateModified from './_a-date-modified';

const getJson = (url, success, error) => {
  let request = new XMLHttpRequest();
  request.open('GET', url, true);

  request.onload = function() {
    success( JSON.parse(request.responseText) );
  };

  request.onerror = function(e) {
    // There was a connection error of some sort
    error(e);
  };

  request.onreadystatechange = (e) => {
    dateModified(request);

    if (request.status === 404 || request.status === 500) {
      // There was a connection error that returned 404 or 500
      error(e);
    }
  };

  request.send();
};

const createLink = (isDirect, item) => {
  const guidePath = 'page.html',
    isExternalPath = /^(https?:)?\/\//.test(item.href),
    _href = (item.href === '#' || isExternalPath) ? item.href
      : (isDirect) ? `../${item.href}`
        : `${guidePath}?p=${item.href}`;

  // Create Anchor Link
  let anchor = document.createElement('a');
  anchor.setAttribute('href', _href);
  anchor.textContent = item.title;

  // Create ID of Item
  let spanID = document.createElement('span');
  spanID.textContent = item.id;
  anchor.appendChild(spanID);

  // Create icon of Item
  let icon = document.createElement('i');
  icon.setAttribute('class', 'material-icons');
  icon.textContent = item.icon;
  anchor.insertBefore(icon, anchor.childNodes[0]);

  return anchor;
};

const createMainList__ITEM = (el, isDirect, data) => {

  for (let i = 0; i < data.length; i++) {
    let listItem = document.createElement('li');
    let anchorLink = createLink(isDirect, data[i]);
    listItem.appendChild(anchorLink);

    el.appendChild(listItem);
  }

  return el;
};

const createMainList = (el, res) => {
  const data = res.dataList,
    is_Direct = res.directLink;

  for (let i = 0; i < data.length; i++) {
    // Create wrapper of main list
    let wrapper = document.createElement('div');
    wrapper.setAttribute('class', 'gd-main-list');
    el.appendChild(wrapper);

    // Create title of main list
    let title = document.createElement('h3');
    title.setAttribute('class', 'gd-main-list__ttl');
    title.textContent = data[i].name;
    wrapper.appendChild(title);

    // Create icon of Title
    let icon = document.createElement('i');
    icon.setAttribute('class', 'material-icons');
    icon.textContent = data[i].icon;
    title.insertBefore(icon, title.childNodes[0]);

    // Create Main List
    let list = document.createElement('ul');
    list.setAttribute('class', 'gd-main-list__main');
    let LIST_ITEM = createMainList__ITEM(list, is_Direct, data[i].list);
    wrapper.appendChild(LIST_ITEM);
  }

};

const buildList = () => {
  const gdBuildList = document.querySelector('.j-gd-build-list');

  if (gdBuildList) {
    getJson(
      './data/data-list.json',
      (response) => {

        createMainList(gdBuildList, response);
        navActive(gdBuildList);

      }, () => {
        console.error('Guideline build failed');
      }
    );
  }
};

export default buildList;
