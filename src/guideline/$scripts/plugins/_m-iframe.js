const iFrame = () => {
  const path = window.location.href.split('?p=')[1];

  if (path){
    const title = path.split('/')[path.split('/').length - 1].split('.')[0].replace('-', ' ');

    // Set title
    document.getElementById('gd-title').innerHTML = `Guideline - ${title.toUpperCase()}`;
  }

  // let windowWidth = window.innerWidth;

  const frameID = document.getElementById('j-gd-iframe__wrapper');

  // Set width for iframe's wrapper
  // const setwidthInFrame = (width) => {
  //   frameID.style.width = `${width}px`;
  // }

  // Create iframe
  if ( frameID ) {

    // setwidthInFrame(windowWidth);
    let createIframe = document.createElement('iframe');

    createIframe.id = 'j-gd-iframe__main';
    createIframe.src = `../${path}`;
    createIframe.name = 'guidelineframe';
    createIframe.sandbox = 'allow-same-origin allow-scripts allow-popups allow-forms';

    frameID.appendChild(createIframe);

  }

  // Event Handler
  // window.addEventListener( 'resize', () => {
  //   windowWidth = window.innerWidth;

  //   if ( frameID ) {
  //     setwidthInFrame(windowWidth);
  //   }
  // });
};

export default iFrame;
