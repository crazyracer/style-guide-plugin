const saveStateToStorage = (key, value) => {
  sessionStorage.setItem(key, value);
};

const removeStateFromStorage = (key) => {
  sessionStorage.removeItem(key);
};

const loadStateFromStorage = (key, target) => {
  if (sessionStorage.getItem(key)) {
    switch (key) {
      case 'gd-iframe--width':
        target.style.width = sessionStorage.getItem(key);
        break;
      case 'gd-iframe--height':
        target.style.height = sessionStorage.getItem(key);
        break;
      default:
        console.error(`There is no action for sessionStorage "${key}"`);
    }
  }
};

export { saveStateToStorage, removeStateFromStorage, loadStateFromStorage };
