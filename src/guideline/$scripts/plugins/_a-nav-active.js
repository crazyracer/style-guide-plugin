const navActive = (element) => {
  const path = window.location.href.split('?p=')[1];

  if (element) {
    const hrefs = element.querySelectorAll('a');

    if (hrefs) {
      for (let i = 0; i < hrefs.length; i++) {
        const href = hrefs[i].getAttribute('href').split('?p=')[1];

        if (href && href === path) {
          hrefs[i].setAttribute('disabled', 'disabled');
        }
      }
    }
  }
};

export default navActive;
