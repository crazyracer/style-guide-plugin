const collectionHas = (a, b) => { //helper function (see below)
  for (let i = 0, len = a.length; i < len; i ++) {
    if (a[i] === b) {
      return true;
    }
  }
  return false;
};

const findParentBySelector = (elm, selector) => {
  let all = document.querySelectorAll(selector);
  let cur = elm.parentNode;
  while (cur && !collectionHas(all, cur)) { //keep going until you find a match
    cur = cur.parentNode; //go up
  }
  return cur; //will return null if not found
};

export default findParentBySelector;
